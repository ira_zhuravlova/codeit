require('../scss/app.scss');
require('../scss/swiper.min.css');

var $ = require('jquery'),
    Swiper = require('./utils/swiperUtil'),
    modal = $('#myModal');

Swiper();

$('#myBtn').on("click", function(e) {
    e.preventDefault();
    $(modal).fadeIn('slow');
});

$('.modal__close').on("click", function(e) {
    $(modal).fadeOut('slow');
});

$('#myModal').on("click", function(e) {
    $(modal).fadeOut('slow');
});

$('.nav__menu-button').on("click", function(e) {
    $("#menu-list").slideToggle('slow');
});

$('.aside__element-name').on("click", function(e) {
    $(e.target).next('ul').slideToggle('slow');
    $(e.target).toggleClass('aside__element-name--active');
});